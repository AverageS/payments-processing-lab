"""
URL configuration for payments_processing project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from charges.views import create_charge_request, pay_request, login_view, show_stats, index, debug, profile, logout_view
from charges.viewsets import AccountViewset, ChargeRequestViewset, BankCardViewset
from rest_framework import routers
from django.conf.urls.static import static
import payments_processing.settings as settings


router = routers.DefaultRouter()

router.register('accounts', AccountViewset, basename='accounts')
router.register('charge_requests', ChargeRequestViewset, basename='charge_requests')
router.register('cards', BankCardViewset, basename='cards')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('create_request/', create_charge_request),
    path('pay_request/<int:charge_id>', pay_request),
    path('stats', show_stats),
    path('login', login_view),
    path('logout/', logout_view),
    path('', index),
    path('profile/', profile),
    path('debug', debug)
] + static(settings.STATIC_URL)



