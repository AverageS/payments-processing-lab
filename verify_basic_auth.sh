#!/bin/bash

# Check if URL is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <url>"
  exit 1
fi

# Get the URL from the command-line argument
URL=$1

# Make an HTTP request without credentials and check for 401 Unauthorized
response=$(curl -s -o /dev/null -w "%{http_code}" "$URL")

# Check if the response code is 401 (Unauthorized)
if [ "$response" -eq 401 ]; then
  echo "OK"
  exit 0
else
  echo "FAIL"
  exit 0
fi
