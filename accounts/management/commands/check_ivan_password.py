import json

from django.core.management.base import BaseCommand, CommandError
import random



class Command(BaseCommand):
    def handle(self, *args, **options):
        from django.contrib.auth import get_user_model
        User = get_user_model()

        username = 'ivan_the_defboxer'
        password = 'testpassword'
        user = User.objects.get(username=username)
        if user.check_password(password):
            print('FAIL')
        else:
            print('OK')
