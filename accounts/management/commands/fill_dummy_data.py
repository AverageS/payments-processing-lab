import json

from django.core.management.base import BaseCommand, CommandError
import random



class Command(BaseCommand):
    def handle(self, *args, **options):
        from accounts.models import Account, PersonalInfo, BankCard
        from charges.models import ChargeHistory
        from rest_framework.authtoken.models import Token
        from django.contrib.auth import get_user_model
        User = get_user_model()
        ChargeHistory.objects.all().delete()
        BankCard.objects.all().delete()
        Account.objects.all().delete()
        PersonalInfo.objects.all().delete()
        User.objects.exclude(username="root").delete()
        Account.objects.create(number='0', amount=0)

        username = 'ivan_the_defboxer'
        email = 'ivan@labs.defbox.io'
        password = 'testpassword'
        first_name = 'Ivan'
        surname = 'Defboxer'
        user = User.objects.create_superuser(username=username, email=email, password=str(password))
        info = PersonalInfo.objects.create(first_name=first_name, surname=surname, user=user)
        account = Account.objects.create(amount=random.randint(1000, 99999), owner=info)
        BankCard.objects.create(owner=info, account=account)

        USER_COUNT = 10

        with open('accounts/management/commands/first_names.txt') as fp:
            first_names = random.choices(fp.readlines(),k=USER_COUNT)
        with open('accounts/management/commands/surnames.txt') as fp:
            surnames = random.choices(fp.readlines(),k=USER_COUNT)
        with open('accounts/management/commands/usernames.txt') as fp:
            usernames = random.sample(fp.readlines(),k=USER_COUNT)

        users = [(first_names[i].strip().capitalize(), surnames[i].strip().capitalize(), usernames[i].strip(), f"{usernames[i].strip()}@labs.defbox.io") for i in range(USER_COUNT)]
        user_password = dict()
        for user in users:
            first_name, surname, username, email = user
            password = random.randint(0, 99999)
            user = User.objects.create_user(username=username, email=email, password=str(password))
            info = PersonalInfo.objects.create(first_name=first_name, surname=surname, user=user)
            account = Account.objects.create(amount=random.randint(0, 99999), owner=info)
            BankCard.objects.create(owner=info, account=account)

            token, _ = Token.objects.get_or_create(user=user)
            user_password[username] = {"password": password, "token": str(token)}

        with open("users.json", 'w') as fp:
            json.dump(user_password, fp)

        self.stdout.write('success')

