from django.db import models
from django.contrib.auth import get_user_model

import random
import string

# Create your models here.
User = get_user_model()
def generate_account_number():
    return f"MS001337{''.join(random.choices(string.digits, k=9))}"

def generate_card_number():
    numbers = ''.join(random.choices(string.digits, k=9))
    def digits_of(n):
        return [int(d) for d in str(n)]
    digits = digits_of(numbers)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d*2))
    check_digit = checksum % 10

    return f"13371337{numbers}{check_digit}"

def generate_cvc():
    numbers = ''.join(random.choices(string.digits, k=3))
    return numbers

class Account(models.Model):
    number = models.CharField(max_length=34,unique=True,default=generate_account_number)
    amount = models.DecimalField(max_digits=30, decimal_places=9)
    owner = models.ForeignKey('accounts.PersonalInfo', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.number}"


class PersonalInfo(models.Model):
    first_name = models.CharField(max_length=64)
    surname = models.CharField(max_length=64)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"{self.first_name} {self.surname}"


class BankCard(models.Model):
    number = models.CharField(max_length=19, default=generate_card_number, unique=True)
    cvc = models.CharField(max_length=3, default=generate_cvc)
    owner = models.ForeignKey(PersonalInfo, on_delete=models.PROTECT)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)



