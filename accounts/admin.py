from django.contrib import admin
from accounts.models import Account, BankCard, PersonalInfo
# Register your models here.

class PersonalInfoAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'surname']

class AccountAdmin(admin.ModelAdmin):
    search_fields = ['number']

class BankCardAdmin(admin.ModelAdmin):
    autocomplete_fields = ['owner', 'account']

admin.site.register(Account, AccountAdmin)
admin.site.register(BankCard, BankCardAdmin)
admin.site.register(PersonalInfo, PersonalInfoAdmin)
