#!/bin/bash

# Check if a URL is provided as a parameter
if [ -z "$1" ]; then
    echo "Usage: $0 <URL>"
    exit 1
fi

# URL to check (from the first argument)
URL="$1"

# Send a HEAD request to check if the URL returns a 200 response
HTTP_RESPONSE=$(curl -o /dev/null -s -w "%{http_code}\n" "$URL")

# Check if the response is 200
if [ "$HTTP_RESPONSE" -eq 200 ]; then
    echo "FAIL"
else
    echo "OK"
fi
