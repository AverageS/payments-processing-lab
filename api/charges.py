import requests


class Bank():
    def __init__(self, base_url ,token):
        self.token = token
        self.headers = {
            "Authorization": f"Token {token}"
        }
        self.base_url = base_url

        r =  requests.get(f"{self.base_url}api/v1/accounts/",
                          headers=self.headers)

        data = r.json()[0]
        self.amount = data['amount']
        self.account_number = data['number']
        self.account_id = data['id']

        r = requests.get(f"{self.base_url}api/v1/cards/",
                         headers=self.headers)
        data = r.json()[0]

        self.card = {
            "number": data['number'],
            "cvc": data['cvc']
        }

    def create_charge(self, amount):
        r = requests.post(f"{self.base_url}api/v1/accounts/{self.account_id}/create_charge/",
                          headers=self.headers,
                          json={"amount": amount})
        return r.json()['uuid']

    def pay(self, charge_request):
        r = requests.post(f"{self.base_url}api/v1/charge_requests/{charge_request}/pay/",
                          headers=self.headers,
                          json={
                              "card_number": self.card['number'],
                              "cvc": self.card['cvc']
                          })
        print(r.json())


    def get_my_amount(self):
        r =  requests.get(f"{self.base_url}api/v1/accounts/",
                          headers=self.headers)


        return r.json()[0]['amount']
