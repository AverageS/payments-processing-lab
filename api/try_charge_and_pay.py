import time

from charges import Bank
import random
import json

BASE_URL = "http://backend:8000/"

MAXIMUM_AMOUNT = 10000

charges = []
with open('users.json') as fp:
    users = json.load(fp)

usernames = list(users.keys())
while True:
    user_to_choose = random.randint(0, len(usernames) - 1 )
    user = usernames[user_to_choose]
    bank = Bank(BASE_URL, users[user]['token'])

    amount = random.randint(0, MAXIMUM_AMOUNT)

    if not charges:
        create_charge = True
    elif len(charges) > 10:
        create_charge = False
    else:
        create_charge = random.randint(0,10) < 7

    if create_charge:
        charge = bank.create_charge(amount)
        charges.append(charge)
        print(f"{user} CREATED {charge}")
    else:
        charge = charges.pop()
        bank.pay(charge)
        print(f"{user} PAID {charge}")
    time.sleep(1)

