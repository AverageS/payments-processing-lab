Run the project locally: 
`docker compose up`

Run DB only 
`docker compose -f develop-compose up`

In case anything happens in prod you can ssh there (dont forget to change $IP):
`ssh -i key root@$IP` 

User `ivan_the_defboxer@testpassword`