from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.db import transaction
from django.db.models import Q
import charges.models as charges
import accounts.models as accounts


from decimal import Decimal

from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

# Create your views here.
@login_required(login_url='/login')
def create_charge_request(request):
    import accounts.models as accounts

    if request.method == 'POST':
        to_number = request.POST.get('to').strip()
        account_to = accounts.Account.objects.get(number=to_number)
        amount = request.POST.get('amount')
        charge = charges.ChargeRequest.objects.create(to=account_to, amount=amount)
        context = {"amount": amount, "location": request.build_absolute_uri(f'/pay_request/{charge.id}')}
        return render(request, "request.html", context)
    else:
        accounts = [x.number for x in accounts.Account.objects.filter(owner__user=request.user)]
        return render(request, "create_request.html", {'accounts': accounts})

@transaction.atomic
@login_required(login_url='/login')
def pay_request(request, charge_id):
    charge = charges.ChargeRequest.objects.get(id=charge_id)
    if charge.paid:
        return render(request, "pay_request.html", {})
    if request.method == 'POST':
        card_number = request.POST.get('card_number')
        cvc = request.POST.get('cvc')
        card = accounts.BankCard.objects.get(number=card_number, cvc=cvc)
        account_from = card.account
        account_to = charge.to
        bank_account = accounts.Account.objects.get(number=0)
        fee = charge.amount*Decimal(0.04)
        if account_from.amount < (charge.amount + fee):
            context = {"failed": True}
            return render(request, "paid_fail.html", context)
        account_from.amount -= (charge.amount + fee)
        account_to.amount += charge.amount
        bank_account.amount += fee
        account_to.save()
        account_from.save()
        bank_account.save()
        charge.paid = True
        charge.save()
        charges.ChargeHistory.objects.create(charge=charge, card=card)
        return render(request, 'paid_success.html', {"charge_id": charge_id})
    random_card = accounts.BankCard.objects.all().first()

    context = {
        "charge_id": charge.id,
        "to": charge.to.number,
        "amount": charge.amount,
        "paid": charge.paid,
        "card_number": random_card.number,
        "cvc": random_card.cvc
    }
    return render(request, "pay_request.html", context)


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
    if request.user.is_authenticated:
        return redirect('/')
    else:
        context = {}
        return render(request, 'index.html', context)



def show_stats(request):
    history = charges.ChargeHistory.objects.all()
    bank_profit = accounts.Account.objects.get(number='0').amount
    context = {
        'charges': history,
        'bank_profit': bank_profit
    }
    return render(request, 'stats.html', context)


def index(request):
    return render(request, 'index.html', {'user': request.user})


def debug(request):
    context = {"amount": 100, "location": request.build_absolute_uri(f'/pay_request/{1}')}
    return render(request, "request.html", context)


@login_required(login_url='/login')
def profile(request):
    user = request.user
    random_card = accounts.BankCard.objects.filter(owner__user=user).first()
    transactions = charges.ChargeHistory.objects.filter(Q(charge__to__owner__user=user) | Q(card__owner__user=user)).all()
    return render(request, 'profile.html', {'card': random_card, 'transactions': transactions})


def logout_view(request):
    logout(request)
    return redirect('/')