from rest_framework import serializers
from accounts.models import Account, BankCard
from charges.models import ChargeRequest

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'number', 'amount']


class BankCardSerializer(serializers.ModelSerializer):

    account = serializers.StringRelatedField()

    class Meta:
        model = BankCard
        fields = ['number', 'cvc', 'account']


class ChargeRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargeRequest
        fields = ['uuid','amount', 'to', 'paid']