from django.contrib import admin
from charges.models import ChargeRequest, ChargeHistory

# Register your models here.
class ChargeRequestAdmin(admin.ModelAdmin):
    list_display = ['to', 'amount', 'paid']


admin.site.register(ChargeRequest, ChargeRequestAdmin)
admin.site.register(ChargeHistory)