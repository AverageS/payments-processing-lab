from rest_framework import viewsets
from rest_framework.decorators import action
from django.http import JsonResponse
from django.db import transaction

from accounts.models import Account, BankCard
from charges.models import ChargeRequest
from charges.serializers import AccountSerializer, ChargeRequestSerializer, BankCardSerializer
import accounts.models as accounts

from decimal import Decimal
import charges.models as charges


class AccountViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_queryset(self):
        user = self.request.user
        return Account.objects.filter(owner__user=user)

    @action(detail=True, methods=['POST'])
    def create_charge(self, request, pk):
        account_to = self.get_object()
        amount = request.data.get('amount')
        charge = ChargeRequest.objects.create(to=account_to, amount=amount)
        context = {"amount": amount, "location": request.build_absolute_uri(f'/pay_request/{charge.id}'),
                                                                            "uuid": charge.uuid}
        return JsonResponse(context)


class BankCardViewset(viewsets.ReadOnlyModelViewSet):
    queryset = BankCard.objects.all()
    serializer_class = BankCardSerializer

    def get_queryset(self):
        user = self.request.user
        return BankCard.objects.filter(owner__user=user)

class ChargeRequestViewset(viewsets.ModelViewSet):
    queryset = ChargeRequest.objects.all()
    serializer_class = ChargeRequestSerializer

    lookup_field = 'uuid'

    def get_queryset(self):
        if self.action in ['retrieve', 'pay']:
            return ChargeRequest.objects.all()
        return ChargeRequest.objects.filter(amount__lte=0)

    @action(detail=True, methods=['post'])
    @transaction.atomic
    def pay(self, request, uuid):
        charge = self.get_object()
        card_number = request.data.get('card_number')
        cvc = request.data.get('cvc')
        card = accounts.BankCard.objects.get(number=card_number, cvc=cvc)
        account_from = card.account
        account_to = charge.to
        bank_account = accounts.Account.objects.get(number=0)
        fee = charge.amount * Decimal(0.04)
        if account_from.amount < (charge.amount + fee):
            return JsonResponse({"status": "fail", "reason": "Not enough money"}, status=400)
        account_from.amount -= (charge.amount + fee)
        account_to.amount += charge.amount
        bank_account.amount += fee
        account_to.save()
        account_from.save()
        bank_account.save()
        charge.paid = True
        charge.save()
        charges.ChargeHistory.objects.create(charge=charge, card=card)

        context = {
            "charge_id": charge.id,
            "to": charge.to.number,
            "amount": charge.amount,
            "paid": charge.paid
        }
        return JsonResponse(context)

