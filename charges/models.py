from django.db import models
import uuid
# Create your models here.

class ChargeRequest(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    amount = models.DecimalField(max_digits=30, decimal_places=9)
    to = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    paid = models.BooleanField(default=False)


class ChargeHistory(models.Model):
    charge = models.ForeignKey(ChargeRequest, on_delete=models.CASCADE)
    card = models.ForeignKey('accounts.BankCard', on_delete=models.PROTECT)